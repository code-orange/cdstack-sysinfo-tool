import configparser

import netifaces

config = configparser.ConfigParser()

ini_str = "[root]\n" + open("/etc/cmdb/cmdb_deploy.conf", "r").read()

config.read_string(ini_str)

host_id = str(config["root"]["api_username"])

sql_str = "INSERT INTO `cmdb_vars_host` (`name`, `value`, `host_rel_id`) VALUES ('{}','{}','{}');\n"
sql = str()

for interface in netifaces.interfaces():
    print("Current Interface: " + interface + "\n")

    ip_settings_full = netifaces.ifaddresses(interface)

    if interface.startswith("lo") or netifaces.AF_LINK not in ip_settings_full:
        print("Skip Interface: " + interface + "\n")
        continue

    phy_settings = ip_settings_full[netifaces.AF_LINK][0]

    if interface.startswith("tun") or interface.startswith("tap"):
        print("Skip IP: " + interface + "\n")
        sql += sql_str.format(
            "network_iface_" + interface + "_hwaddr", phy_settings["addr"], host_id
        )
        sql += sql_str.format(
            "network_iface_" + interface + "_fw_policy_allow", "all", host_id
        )
        sql += sql_str.format("network_iface_" + interface + "_fw_zone", "loc", host_id)
        continue

    sql += sql_str.format(
        "network_iface_" + interface + "_hwaddr", phy_settings["addr"], host_id
    )
    sql += sql_str.format(
        "network_iface_" + interface + "_fw_policy_allow", "all", host_id
    )
    sql += sql_str.format("network_iface_" + interface + "_fw_zone", "loc", host_id)

    if interface == "eth5":
        sql += sql_str.format("network_iface_" + interface + "_mode", "ppp", host_id)
        sql += sql_str.format("network_iface_" + interface + "_ppp_unit", "0", host_id)
        sql += sql_str.format("network_iface_" + interface + "_fw_zone", "net", host_id)
        sql += sql_str.format("network_iface_" + "ppp0" + "_fw_zone", "net", host_id)

    if netifaces.AF_INET not in ip_settings_full:
        print("Skip Interface: " + interface + "\n")
        continue

    ip_settings = ip_settings_full[netifaces.AF_INET][0]

    sql += sql_str.format(
        "network_iface_" + interface + "_ip", ip_settings["addr"], host_id
    )
    sql += sql_str.format(
        "network_iface_" + interface + "_netmask", ip_settings["netmask"], host_id
    )

    sql += sql_str.format(
        "network_iface_" + interface + "_fw_snat_to", "ppp0,tun10", host_id
    )

sql += sql_str.format("firewall_zone_custom_01_name", "rds", host_id)
sql += sql_str.format("firewall_zone_custom_01_ifaces", "tun10", host_id)
sql += sql_str.format("firewall_zone_custom_01_fw_policy_allow", "all", host_id)

print(sql)
